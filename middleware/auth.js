let jwt = require('jsonwebtoken')
const User = require('../models/user')

verifyToken = (req, res, next) => {
  let token = req.headers.authorization
  if (token) {
    jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
      try {
        req.userData = decoded
        // res.send(req.userData)
        next()
      } catch (err) {
        return res.json({
          success: false,
          message: 'Token is not valid'
        })
      }
    })

  } else {
    return res.status(401).send({
      success: false,
      message: 'Unauthenticated'
    })
  }
};

const authJwt = {
  verifyToken
};
module.exports = authJwt;