require('dotenv').config()

const express = require('express');
const app = express();
const cors = require('cors');
const db = require("./config/db")
const sequelize = require('./models');

dbConnection()
sequelize.sync({ alter: true, force: true});


// route
const Routers = require('./routes/index');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// express use route
app.use('/api', Routers);

async function dbConnection() {
  try {
    await sequelize.authenticate();
    console.log('Connection has been established');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
}

// Handling Errors
app.use((err, req, res, next) => {
    err.statusCode = err.statusCode || 500;
    err.message = err.message || "Internal Server Error";
    res.status(err.statusCode).json({
      message: err.message,
    });
});

app.listen(3001, () => console.log('port start in 3001'))