const {DataTypes} = require('sequelize');
const sequelize = require('../models');
const Absent = require('./absent')


const User = sequelize.define('user', {
  username: {
    type: DataTypes.STRING(200),
    unique: true,
    allowNull: false
  },
  name: {
    type: DataTypes.STRING(30),
    allowNull: false
  },
  email: {
    type: DataTypes.STRING(200),
    allowNull: true
  },
  password: {
    type: DataTypes.STRING(200),
    allowNull: true
  },
}, {
  freezeTableName: true
})

Absent.hasMany(Absent, {foreignKey: 'user_id'})

module.exports = User;