const {DataTypes} = require('sequelize');
const sequelize = require('../models');
const user = require("./user");


const Absent = sequelize.define('absent', {
  absent_in: {
    type: DataTypes.DATE(),
    unique: true,
    allowNull: false
  },
  absent_out: {
    type: DataTypes.DATE(),
    allowNull: true
  },
  date: {
    type: DataTypes.DATEONLY(),
    allowNull: false
  },
}, {
  freezeTableName: true
})

Absent.belongsTo(Absent, {foreignKey: 'user_id'})

module.exports = Absent;