const dbConfig = require("../config/db");
const { Sequelize } = require('sequelize');

const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: 'localhost',
  port: 3306,
  dialect: 'mysql'
})

module.exports = sequelize