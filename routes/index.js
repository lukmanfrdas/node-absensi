const express = require('express');
const router = express.Router();
const UserController = require('../controller/UserController');
const AbsentController = require('../controller/AbsentController');

const auth = require('../middleware/auth')

router.route('/absent-in')
    .post([auth.verifyToken],AbsentController.in)

router.route('/absent-out')
    .post([auth.verifyToken],AbsentController.out)

router.route('/absent-data')
    .get([auth.verifyToken],AbsentController.data)

router.route('/absent/list')
    .get([auth.verifyToken],AbsentController.list)

router.route('/absent-validation')
    .get(AbsentController.validation)

router.route('/login')
    .post(UserController.login)

router.route('/logout')
    .post([auth.verifyToken],UserController.logout)

router.route('/register')
    .post(UserController.register)

module.exports = router;