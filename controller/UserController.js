const User = require('../models/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const {validationResult} = require('express-validator');

exports.register = async function (req, res) {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({errors: errors.array()});
  }

  try {
    const validationEmail = await User.findOne({
      where: {email: req.body.email}
    })

    if (validationEmail === null) {
      const {
        name,
        username,
        email,
      } = req.body;

      const hashPass = await bcrypt.hash(req.body.password, 12);

      const newUser = new User({
        name,
        username,
        email,
        password: hashPass
      });

      await newUser.save();

      return res.json({
        code: 200,
        message: 'Successfully',
        data: newUser
      });
    } else {
      return res.status(201).json({
        code: 201,
        message: "The email already created",
      });
    }
  } catch (err) {
    res.json({
      code: 500,
      message: err.message
    });
  }
}

exports.login = async function (req, res) {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).json({errors: errors.array()});
  }

  try {
    let userData = await User.findOne({
      where: {email: req.body.email}
    })
    console.log(userData)
    if (userData != null) {

      const passMatch = await bcrypt.compare(req.body.password, userData.password);

      if (!passMatch) {
        return res.status(422).json({
          message: "Incorrect password",
        });
      }

      const token = jwt.sign(
          {id: userData.id},
          process.env.SECRET_KEY,
          {expiresIn: '1h'}
          );

      return res.json({
        code: 200,
        message: 'Successfully',
        data: userData,
        token: token
      });
    } else {
      return res.json({
        code: 200,
        message: 'Successfully',
        data: userData,
        token: token
      });
    }
  } catch (err) {
    res.json({
      code: 500,
      message: err.message
    });
  }
}

exports.logout = async function (req, res) {
  res.send(req.userData)
}

