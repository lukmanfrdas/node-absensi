const Absent = require('../models/absent');
const jwt = require('jsonwebtoken');
const {validationResult} = require('express-validator');

exports.in = async function (req, res) {

    try {
        console.log(req.body.user_id)
        let ts = Date.now();
        let date_ob = new Date(ts);
        let date = date_ob.getDate();
        let month = date_ob.getMonth();
        let year = date_ob.getFullYear();
        let hours = date_ob.getHours();
        let minutes = date_ob.getMinutes();
        let seconds = date_ob.getSeconds();

        const dateNow = year + "-" + month + "-" + date;

        const dateNowIn = year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;

        console.log(dateNowIn);

        const absentIn = new Absent({
            user_id: req.body.user_id,
            absent_in:dateNowIn,
            date:dateNow
        });

        await absentIn.save();
        
        res.json({ 
            code: 200,
            message: 'Successfully',
            data: absentIn
        });
    } catch (err) {
        res.json({
            code: 500,
            message: err.message
        });
    }
}

exports.out = async function (req, res) {

    try {        
        console.log(req.body.id);

        let ts = Date.now();
        let date_ob = new Date(ts);
        let date = date_ob.getDate();
        let month = date_ob.getMonth();
        let year = date_ob.getFullYear();
        let hours = date_ob.getHours();
        let minutes = date_ob.getMinutes();
        let seconds = date_ob.getSeconds();  
        
        const dateNowout = year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;
        
        const dateNow = year + "-" + month + "-" + date;      

        const absentOut = await Absent.update({
            absent_out:dateNowout
        }, {
            where: {
                user_id: req.body.user_id,
                date:dateNow
            }
        });
        
        res.json({ 
            code: 200,
            message: 'Successfully',
            data: absentOut
        });
        
    } catch (err) {
        res.json({ 
            code: 500,
            message: err.message 
        });
    }
}

exports.data = async function (req, res) {

    try {        

        let ts = Date.now();
        let date_ob = new Date(ts);
        let date = date_ob.getDate();
        let month = ("0" + (date_ob.getMonth())).slice(-2);
        let year = date_ob.getFullYear();  
        const dateNow = year + "-" + month + "-" + date;      
        
        console.log(dateNow);
        
        const data = await Absent.findOne({
            where: {
                user_id:req.body.user_id,
                date:dateNow
            }
        })
        
        res.json({ 
            code: 200,
            message: 'Successfully',
            data: data
        });
        
    } catch (err) {
        res.json({ 
            code: 500,
            message: err.message 
        });
    }
}


exports.validation = async function (req, res) {

    try {        

        let ts = Date.now();
        let date_ob = new Date(ts);
        let date = date_ob.getDate();
        let month = ("0" + (date_ob.getMonth())).slice(-2);
        let year = date_ob.getFullYear();  
        const dateNow = year + "-" + month + "-" + date;      
        
        console.log(dateNow);
        
        const data = await Absent.findOne({
            where: {
                user_id:req.body.user_id,
                date:dateNow
            }
        })
        
        console.log(data)

        if (data == null) {
            return res.json({ 
                code: 200,
                message: 'Anda Belum Absent'
            });                
        }

        if (data.absent_in == null) {
            return res.json({ 
                code: 200,
                message: 'Anda Belum Absent Masuk'
            });                
        }

        if (data.absent_out == null) {
            return res.json({ 
                code: 200,
                message: 'Anda Belum Absent Keluar'
            });                
        }

        if (data.absent_out == null) {
            return res.json({ 
                code: 200,
                message: 'Anda Sudah Selesai Absent'
            });                
        }
        
        if (data != null) {
            return res.json({ 
                code: 200,
                message: 'Anda Sudah Selesai Absent'
            });                
        }
    } catch (err) {
        res.json({ 
            code: 500,
            message: err.message 
        });
    }
}

exports.list = async function(req, res){
    if (req.query.page) {
        let limit = 10
        let offset = (req.query.page - 1) * limit
        let absentDatas =  await Absent.findAndCountAll({
            offset: offset,
            limit: limit,
            order: [
                ['date', 'DESC']
            ]
        })
        return res.status(200).json({
            status: true,
            message: "Success",
            data: absentDatas.rows,
            count: absentDatas.count
        })
    } else {
        let limit = 10
        let absentDatas =  await Absent.findAndCountAll({
            limit: limit,
            order: [
                ['date', 'DESC']
            ]
        })
        return res.status(200).json({
            status: true,
            message: "Success",
            data: absentDatas.rows,
            count: absentDatas.count
        })
    }
    
}